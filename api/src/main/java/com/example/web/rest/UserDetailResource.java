package com.example.web.rest;

import com.example.model.UserDetail;
import com.example.service.UserDetailService;
import com.example.service.dto.UserDetailDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;

/**
 * Created by Bikash on 9/01/2017.
 */
@RestController
public class UserDetailResource {

    @Inject
    UserDetailService userDetailService;

    @RequestMapping(
            value = "/api/userdetail",
            method = RequestMethod.POST)
    public ResponseEntity<?>RegisterUserData(@RequestBody UserDetailDTO userDetailDTO){

        UserDetail userDetail = userDetailService.insertDetail(userDetailDTO);
        System.out.println(userDetail.toString());
        return new ResponseEntity<>(userDetail,HttpStatus.CREATED);
    }
}
