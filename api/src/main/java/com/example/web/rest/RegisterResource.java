package com.example.web.rest;

import com.example.model.User;
import com.example.service.UserService;
import com.example.service.dto.UserDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;

/**
 * Created by Bikash on 27/12/2016.
 */

@RestController

public class RegisterResource {

    @Inject
    UserService userService;

    @RequestMapping(value = "/api/Register",method = RequestMethod.POST)
    public ResponseEntity<?> registerUser(@RequestBody UserDTO userDTO){

        System.out.println("Entered api");
        System.out.println(userDTO.toString());
        User user = userService.createUser(userDTO);
//        userService.createUser(userDTO);
        System.out.println("returned value "+userDTO.toString());
//        System.out.println(user.toString());
        return new ResponseEntity(user,HttpStatus.CREATED);
    }

    @RequestMapping(value = "/api/view",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> displayUser(){
        System.out.println("Got into the get method");
        String sth = "Pushkar lasty darayo";
        return  new ResponseEntity<Object>(sth,HttpStatus.CREATED);
    }

}
