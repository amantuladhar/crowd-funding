package com.example.service;

import com.example.model.UserDetail;
import com.example.repository.UserDetailRepository;
import com.example.repository.UserRepository;
import com.example.service.dto.UserDetailDTO;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import javax.transaction.Transactional;

/**
 * Created by Bikash on 9/01/2017.
 */
@Service
@Transactional
public class UserDetailService {

    @Inject
    UserRepository userRepository;

    @Inject
    UserDetailRepository userDetailRepository;

    public UserDetail insertDetail(UserDetailDTO userDetailDTO){
        UserDetail userDetail = new UserDetail();
        System.out.println(userDetailDTO.toString());
        userDetail.setAddress(userDetailDTO.getAddress());
        userDetail.setUser(userDetailDTO.getUser());
        userDetail.setName(userDetailDTO.getName());
        userDetail.setGender(userDetailDTO.getGender());
        userDetail.setPhoneNumber(userDetailDTO.getPhoneNumber());

        if(userDetailRepository.getOneByUser(userDetailDTO.getUser()).isPresent()){
            System.out.println("User detail is already inserted");
            userDetail=userDetailRepository.findByAccountID(userDetailDTO.getUser().getAccountID());

        }else
        {

            userDetailRepository.save(userDetail);
        }

        return userDetail;
    }
}
