package com.example.service.dto;

import com.example.model.Project;
import com.example.model.Status;
import com.example.model.User;

/**
 * Created by Bikash on 22/12/2016.
 */
public class ProjectDTO {

    int id;

    Status status;

    User user;

    public ProjectDTO() {
    }

    public ProjectDTO(int id, Status status, User user) {
        this.id = id;
        this.status = status;
        this.user = user;
    }

    public ProjectDTO(Project p) {
        this(p.getId(),p.getStatus(),p.getUser());
    }
}
