package com.example.service.dto;

import com.example.model.Address;
import com.example.model.User;
import com.example.model.UserDetail;


/**
 * Created by Bikash on 22/12/2016.
 */
public class UserDetailDTO {

    private int id;
    private String phoneNumber;
    private String name;
    private Address address;
    private String gender;
//    private  String accountID;
    private User user;

    public UserDetailDTO() {
    }

    public UserDetailDTO(int id, String phoneNumber, String name,String gender, Address address, User user) {
        this.id = id;
        this.phoneNumber = phoneNumber;
        this.name = name;
        this.gender = gender;
        this.address = address;
        this.user = user;
//        this.accountID=accountID;
    }

    public UserDetailDTO(UserDetail u) {
        this(u.getId(),u.getPhoneNumber(),u.getName(),u.getGender(),u.getAddress(),u.getUser());
    }

    public int getId() {
        return id;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getName() {
        return name;
    }

    public String getGender() {
        return gender;
    }

    public Address getAddress() {
        return address;
    }

    public User getUser() {
        return user;
    }

//    public String getAccountID() {
//        return accountID;
//    }

    @Override
    public String toString() {
        return "UserDetailDTO{" +
                "id=" + id +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", name='" + name + '\'' +
                ", address=" + address +
                ", gender='" + gender + '\'' +
                ", user=" + user +
                '}';
    }
}
