package com.example.service.dto;

import com.example.model.Category;
import com.example.model.Donation;
import com.example.model.Project;
import com.example.model.ProjectInformation;

import java.util.Collection;

/**
 * Created by Bikash on 22/12/2016.
 */
public class ProjectInformationDTO {

    int id;
    double requireAmount;
    int duration;
    String projectName;
    String photo;
    String videoLink;
    String projectDetail;

    Project project;

    Category category;

    Collection<Donation> donations;

    public ProjectInformationDTO() {
    }

    public ProjectInformationDTO(int id, double requireAmount, int duration, String projectName, String photo, String videoLink, String projectDetail, Project project, Category category, Collection<Donation> donations) {
        this.id = id;
        this.requireAmount = requireAmount;
        this.duration = duration;
        this.projectName = projectName;
        this.photo = photo;
        this.videoLink = videoLink;
        this.projectDetail = projectDetail;
        this.project = project;
        this.category = category;
        this.donations = donations;
    }

    public ProjectInformationDTO(ProjectInformation projectInformation) {

        this(projectInformation.getId(),projectInformation.getRequireAmount(),projectInformation.getDuration(),projectInformation.getProjectName(),projectInformation.getPhoto(),projectInformation.getVideoLink(),projectInformation.getProjectDetail(),projectInformation.getProject(),projectInformation.getCategory(),projectInformation.getDonations());

    }

    public int getId() {
        return id;
    }

    public double getRequireAmount() {
        return requireAmount;
    }

    public int getDuration() {
        return duration;
    }

    public String getProjectName() {
        return projectName;
    }

    public String getPhoto() {
        return photo;
    }

    public String getVideoLink() {
        return videoLink;
    }

    public String getProjectDetail() {
        return projectDetail;
    }

    public Project getProject() {
        return project;
    }

    public Category getCategory() {
        return category;
    }

    public Collection<Donation> getDonations() {
        return donations;
    }

    @Override
    public String toString() {
        return "ProjectInformationDTO{" +
                "id=" + id +
                ", requireAmount=" + requireAmount +
                ", duration=" + duration +
                ", projectName='" + projectName + '\'' +
                ", photo='" + photo + '\'' +
                ", videoLink='" + videoLink + '\'' +
                ", projectDetail='" + projectDetail + '\'' +
                ", project=" + project +
                ", category=" + category +
                ", donations=" + donations +
                '}';
    }
}
