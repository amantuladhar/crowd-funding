package com.example.service.dto;

import com.example.model.Donation;
import com.example.model.Status;
import com.example.model.User;
import com.example.model.UserType;

import java.util.Collection;

/**
 * Created by Bikash on 22/12/2016.
 */
public class UserDTO {

    private Integer id;
    private String accountID;
    private String email;
    private UserType userType;
    private Status status;
    private Collection<Donation> donations;


    public UserDTO() {
    }

    public UserDTO(User u) {
        this(u.getId(),u.getAccountID(),u.getEmail(),u.getUserType(),u.getStatus(),u.getDonations());
    }


    public UserDTO(Integer id, String accountID, String email, UserType userType, Status status, Collection<Donation> donations) {
        this.id = id;
        this.accountID = accountID;
        this.email = email;
        this.userType = userType;
        this.status = status;
        this.donations = donations;
    }

    public int getId() {
        return id;
    }

    public String getAccountID() {
        return accountID;
    }

    public String getEmail() {
        return email;
    }

    public UserType getUserType() {
        return userType;
    }

    public Status getStatus() {
        return status;
    }

    public Collection<Donation> getDonations() {
        return donations;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "UserDTO{" +
                "id=" + id +
                ", accountID=" + accountID +
                ", email='" + email + '\'' +
                ", userType=" + userType +
                ", status=" + status +
                ", donations=" + donations +
                '}';
    }
}
