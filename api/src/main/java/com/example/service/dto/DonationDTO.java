package com.example.service.dto;

import com.example.model.Donation;

/**
 * Created by Bikash on 22/12/2016.
 */
public class DonationDTO {

    int id;
    double amount;
    String message;
    String date;

    public DonationDTO() {
    }

    public DonationDTO(int id, double amount, String message, String date) {
        this.id = id;
        this.amount = amount;
        this.message = message;
        this.date = date;
    }

    public DonationDTO(Donation donation) {

        this(donation.getId(),donation.getAmount(),donation.getMessage(),donation.getDate());
    }

    public int getId() {
        return id;
    }

    public double getAmount() {
        return amount;
    }

    public String getMessage() {
        return message;
    }

    public String getDate() {
        return date;
    }

    @Override
    public String toString() {
        return "DonationDTO{" +
                "id=" + id +
                ", amount=" + amount +
                ", message='" + message + '\'' +
                ", date='" + date + '\'' +
                '}';
    }
}
