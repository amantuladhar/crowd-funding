package com.example.service;

import com.example.model.User;
import com.example.repository.StatusRepository;
import com.example.repository.UserRepository;
import com.example.repository.UserTypeRepository;
import com.example.service.dto.UserDTO;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.Optional;

/**
 * Created by Bikash on 8/01/2017.
 */
@Service

public class UserService {

    @Inject
    UserRepository userRepository;

    @Inject
    UserTypeRepository userTypeRepository;

    @Inject
    StatusRepository statusRepository;

    public User createUser(UserDTO userDTO){

        User user=new User();

        System.out.println("In service"+userDTO.toString());
        user.setAccountID(userDTO.getAccountID());
        user.setEmail(userDTO.getEmail());
        user.setUserType(userTypeRepository.findOneBytype("user"));
        user.setStatus(statusRepository.findOneBystatus("verified"));
        System.out.println(user.toString());
        if(userRepository.findOneByaccountID(userDTO.getAccountID()).isPresent()){

            System.out.println("user found just login");
            user=userRepository.findByaccountID(userDTO.getAccountID());

        }else{
            System.out.println("create new user");
            userRepository.save(user);

        }

        return user;

    }

}
