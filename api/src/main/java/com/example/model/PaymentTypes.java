package com.example.model;

import javax.persistence.*;

/**
 * Created by ak on 12/20/2016.
 */
@Entity
@Table(name = "pamentTypes")
public class PaymentTypes {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "paymentTypeId")
    int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "PaymentTypes{" +
                "id=" + id +
                '}';
    }
}
