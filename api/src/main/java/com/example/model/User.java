package com.example.model;


import javax.persistence.*;
import java.util.Collection;

/**
 * Created by ak on 12/20/2016.
 */
@Entity
@Table(name = "user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "userId")
    private Integer id;
    private String accountID;
    private String email;
    @ManyToOne
    @JoinColumn(name = "userTypeId")
    UserType userType;

    @ManyToOne
    @JoinColumn(name = "statusId")
    Status status;

    @ManyToMany
    Collection<Donation> donations;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAccountID() {
        return accountID;
    }

    public void setAccountID(String accountID) {
        this.accountID = accountID;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Collection<Donation> getDonations() {
        return donations;
    }

    public void setDonations(Collection<Donation> donations) {
        this.donations = donations;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", accountID='" + accountID + '\'' +
                ", email='" + email + '\'' +
                ", userType=" + userType +
                ", status=" + status +
                ", donations=" + donations +
                '}';
    }
}
