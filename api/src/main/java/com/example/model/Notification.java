package com.example.model;

import javax.persistence.*;

/**
 * Created by ak on 12/20/2016.
 */
@Entity
@Table(name = "notification")
public class Notification {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "notificationId")
    int id;
    @ManyToOne
    @JoinColumn(name = "notificationTypeId")
    NotificationTypes notificationTypes;
    @ManyToOne
    @JoinColumn(name = "projectId")
    Project project;

    public NotificationTypes getNotificationTypes() {
        return notificationTypes;
    }

    public void setNotificationTypes(NotificationTypes notificationTypes) {
        this.notificationTypes = notificationTypes;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Notification{" +
                "id=" + id +
                ", notificationTypes=" + notificationTypes +
                ", project=" + project +
                '}';
    }
}
