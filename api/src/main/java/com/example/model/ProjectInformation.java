package com.example.model;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by ak on 12/20/2016.
 */
@Entity
@Table(name = "projectInformation")
public class ProjectInformation {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "informationId")
    int id;
    double requireAmount;
    int duration;
    String projectName;
    String photo;
    String videoLink;
    String projectDetail;
    @OneToOne
    @JoinColumn(name = "projectId")
    Project project;
    @OneToOne
    @JoinColumn(name = "categoryId")
    Category category;
    @ManyToMany
    Collection<Donation> donations;

    public Collection<Donation> getDonations() {
        return donations;
    }

    public void setDonations(Collection<Donation> donations) {
        this.donations = donations;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getRequireAmount() {
        return requireAmount;
    }

    public void setRequireAmount(double requireAmount) {
        this.requireAmount = requireAmount;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getVideoLink() {
        return videoLink;
    }

    public void setVideoLink(String videoLink) {
        this.videoLink = videoLink;
    }

    public String getProjectDetail() {
        return projectDetail;
    }

    public void setProjectDetail(String projectDetail) {
        this.projectDetail = projectDetail;
    }

    @Override
    public String toString() {
        return "ProjectInformation{" +
                "id=" + id +
                ", requireAmount=" + requireAmount +
                ", duration=" + duration +
                ", projectName='" + projectName + '\'' +
                ", photo='" + photo + '\'' +
                ", videoLink='" + videoLink + '\'' +
                ", projectDetail='" + projectDetail + '\'' +
                ", project=" + project +
                ", category=" + category +
                ", donations=" + donations +
                '}';
    }
}
