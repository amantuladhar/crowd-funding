package com.example.model;

import javax.persistence.*;

/**
 * Created by ak on 12/20/2016.
 */
@Entity
@Table(name = "notificationTypes")
public class NotificationTypes {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "notificationTypeId")
    int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "NotificationTypes{" +
                "id=" + id +
                '}';
    }
}
