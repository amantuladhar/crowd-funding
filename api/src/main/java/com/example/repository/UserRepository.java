package com.example.repository;

import com.example.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * Created by Bikash on 8/01/2017.
 */
public interface UserRepository extends JpaRepository<User,Integer>{

    Optional<User> findOneByaccountID(String accountID);
    User findByaccountID(String accountID);
}
