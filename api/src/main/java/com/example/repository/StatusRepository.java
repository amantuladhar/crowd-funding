package com.example.repository;

import com.example.model.Status;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Bikash on 8/01/2017.
 */
public interface StatusRepository extends JpaRepository<Status,Integer>{
    Status findOneBystatus(String status);
}
