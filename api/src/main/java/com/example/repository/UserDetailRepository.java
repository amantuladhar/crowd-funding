package com.example.repository;

import com.example.model.User;
import com.example.model.UserDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

/**
 * Created by Bikash on 9/01/2017.
 */
public interface UserDetailRepository extends JpaRepository<UserDetail,Integer> {

    Optional<UserDetail> getOneByUser(User user);


//    UserDetail findByUser(User user);

    @Query(value = "select c from UserDetail c where c.user.accountID= ?1")
    UserDetail findByAccountID(String accountID);
}
