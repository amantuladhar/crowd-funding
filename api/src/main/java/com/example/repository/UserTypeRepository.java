package com.example.repository;

import com.example.model.UserType;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Bikash on 8/01/2017.
 */
public interface UserTypeRepository extends JpaRepository<UserType,Integer>{
    UserType findOneBytype(String type);
}
