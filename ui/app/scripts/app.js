var app = angular.module('app', [
    'ui.router',
    'ngResource',
    'ui.bootstrap',
    'facebook'

    ]);

app.config(function($stateProvider, $urlRouterProvider,FacebookProvider) {

    FacebookProvider.init('357065717999821');
    $urlRouterProvider.otherwise('');

    $stateProvider

    // HOME STATES AND NESTED VIEWS ========================================
        .state('user', {
            url: '/register',
            views:{

                content: {

                    templateUrl: 'scripts/register/Register.html',
                    controller: 'RegisterController',
                    controllerAs: 'vm'
                }
            }
            // parent:'scripts',

        })
        .state('login', {
            url: '/login',
            views:{
                content:{

                    templateUrl: 'scripts/user/login.html',
                    controller: 'LoginController',
                    controllerAs: 'vm'

                }
            }

        })
        .state('test', {
            url: '/test',
            views:{
                content:{

                    templateUrl: 'scripts/user/test.html',
                    controller: 'testController',
                    controllerAs: 'vm'

                }
            }

        })

        // .state('app', {
        //     'navbar@': {
        //         url: '/',
        //         templateUrl: 'scripts/layout/navbar.html'
        //     }
        //
        // })

        // ABOUT PAGE AND MULTIPLE NAMED VIEWS =================================
        .state('about', {
            // we'll get to this in a bit
        });

});

