/**
 * Created by Bikash on 27/12/2016.
 */
(function() {
    'use strict';

    angular
        .module('app')
        .controller('NavbarController', NavbarController);



    // NavbarController.$inject=['$scope'];
    function NavbarController() {
        var vm = this;
        vm.searchdata = null;
        vm.toggleSearch=toggleSearch;
        vm.check=check;
        vm.resets=resets;
        vm.search=false;


        console.log("Navbar controller");

        function check() {

            console.log("clicked");

        }
        function toggleSearch(){

            vm.search=!(vm.search);
            console.log(vm.search);
            console.log(vm.searchdata);
            vm.searchdata=null;

        }
        function resets() {

            vm.searchdata=null;
            console.log(vm.searchdata);

        }
    }
})();
