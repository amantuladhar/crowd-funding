(function () {
    'use strict';

    angular
        .module('app')
        .factory('UserDetail', UserDetail);

    UserDetail.$inject = ['$resource'];

    function UserDetail($resource) {

        var service = $resource('http://localhost:8080/api/userdetail/:id', {}, {
            'query': {method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'save': { method:'POST' },
            'update': { method:'PUT' },
            'delete':{ method:'DELETE'}
        });

        return service;
    }
})();
