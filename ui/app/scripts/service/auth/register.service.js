(function () {
    'use strict';

    angular
        .module('app')
        .factory('Register', Register);

    Register.$inject = ['$resource'];

    function Register ($resource) {

        var service = $resource('http://localhost:8080/api/Register/:id', {}, {
            'query': {method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'save': { method:'POST' },
            'update': { method:'PUT' },
            'delete':{ method:'DELETE'}
        });

        return service;
    }
})();
