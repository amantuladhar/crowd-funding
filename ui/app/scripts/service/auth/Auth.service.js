/**
 * Created by new on 12/27/2016.
 */
(function() {
    'use strict';

    angular
        .module('app')
        .factory('Auth', Auth);

    Auth.$inject = ['Register','UserDetail'];

    function Auth (Register,UserDetail) {

        var service = {

            createAccount: createAccount,
            createDetails: createDetails

        };

        return service;

        function createAccount (account, callback) {
            var cb = callback || angular.noop;

            console.log("auth ma ni chiryo");

            return Register.save(account,
                function () {
                    return cb(account);
                },
                function (err) {

                    return cb(err);
                }.bind(this)).$promise;
        }

        function createDetails (account, callback) {
            var cb = callback || angular.noop;

            console.log("auth ma ni chiryo");

            return UserDetail.save(account,
                function () {
                    return cb(account);
                },
                function (err) {

                    return cb(err);
                }.bind(this)).$promise;
        }

    }
})();
