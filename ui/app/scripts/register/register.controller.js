(function () {
    'use strict';

    angular
        .module('app')
        .controller('RegisterController', RegisterController);


    RegisterController.$inject = ['Auth', '$scope', 'Facebook', 'CookieService'];

    function RegisterController(Auth, $scope, Facebook, CookieService) {

        var vm = this;

        vm.registerAccount = {};
        vm.user = {};
        vm.userDetail = {};
        vm.loginFb = loginFB;
        vm.Fblogout = Fblogout;
        vm.isLogged = isLogged;
        vm.getUserData = getUserData;
        vm.hasAuthority = hasAuthority;


        function loginFB() {
            // From now on you can use the Facebook service just as Facebook api says
            Facebook.login(function (response) {
                // Facebook.getLoginStatus(function(response) {
                console.log(response);
                if (response.status == "connected") {
                    console.log("Its connected");
                    getUserData();

                } else {
                    console.log("Not connected to facebook");
                    CookieService.logout();
                    // Fblogout();
                }
            });
            // };
        }

        function Fblogout() {

            Facebook.logout(function (response) {
                // Person is now logged out
                console.log("logout button pressed");
                CookieService.logout();
                vm.user = {};

            });
        }

        function getUserData() {
            Facebook.api("/me?fields=id,name,email,gender,hometown", function (account) {

                console.log(account);

                vm.user.accountID = account.id;
                vm.user.email = account.email;
                // vm.userDetail.accountID=account.id;
                vm.userDetail.gender = account.gender;
                vm.userDetail.hometown = account.hometown;
                vm.userDetail.name = account.name;


                Auth.createAccount(vm.user).then(function (responseaccount) {

                    console.log(responseaccount);
                    CookieService.login(responseaccount);
                    vm.userDetail.user=CookieService.user;
                    // console.log("Login vayesi user yesto ayo");
                    // console.dir(CookieService.user);
                    console.log("user Detail: ");
                    console.dir(vm.userDetail);
                    Auth.createDetails(vm.userDetail).then(function (responseaccountdetail) {
                        console.log(responseaccountdetail);
                    }).catch(function (responseaccountdetail) {
                        console.error("error"+responseaccountdetail);
                        Fblogout();
                    })

                }).catch(function (responseaccount) {
                    console.log("error " + responseaccount);
                    Fblogout();
                })
            });
        }

        Facebook.getLoginStatus(function (response) {
            console.log(response);
            if (response.status == "connected") {
                console.log("Its connected");
                getUserData();

            } else {
                console.log("Not connected");
                CookieService.logout();
            }
        });

        function isLogged() {
            return CookieService.loginStatus;
        }

        function hasAuthority() {
            return CookieService.hasAuthority();
        }

    }
})();
